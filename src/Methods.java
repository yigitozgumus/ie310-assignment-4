/**
 * Created by yigitozgumus on 12/16/15.
 * This class will contain the methods for the different solving methods
 *
 */
class Methods {

    public double bisection(double[] coefficients, boolean problemType,double[] intervals, double uncertainty){
        double middlePoint = 0.0;
        double fResult = 0.0 ;
        int iteration  = 0;

        while (intervals[1] - intervals[0] > uncertainty && iteration < 100){
            iteration++;
            middlePoint = (intervals[0] + intervals[1]) /2 ;
            fResult = calculateDerivative(middlePoint,coefficients);

            //if(calculateDerivative(intervals[1],coefficients) * fResult < 0){
                if(fResult < 0) {
                    intervals[problemType?1:0] = middlePoint;// if it is max choose low , min-> up
                    // }else if (calculateDerivative(intervals[0],coefficients) * fResult < 0){
                }else if(fResult > 0){
                intervals[problemType?0:1] = middlePoint ;
            }
        }
        // Do we need to implement a error detection ? or it will give the result no matter what ?
        return middlePoint;
    }
    public double goldenSection(double[] coefficients,boolean problemType,double[] intervals, double uncertainty){
        double r = -0.5 + (Math.sqrt(5)/2);
        while (intervals[1] - intervals[0] > uncertainty ) {
            double middle_1 = intervals[1] - (r * (intervals[1] - intervals[0]));
            double middle_2 = intervals[0] + (r * (intervals[1] - intervals[0]));
            double middle1Result = calculateFx(middle_1,coefficients);
            double middle2Result = calculateFx(middle_2,coefficients);
            if(middle1Result==middle2Result){
                intervals[0] = middle_1;
                intervals[1] = middle_2;
            }else if( (problemType && middle1Result>middle2Result) || (!problemType && middle1Result<middle2Result)){
                intervals[1] = middle_2;
            }else{
                intervals[0] = middle_1;
            }
        }
        double finalResult = (intervals[0] + intervals[1]) /2 ;
        //return analyzeOutput(problemType,coefficients,range,finalResult);
        return finalResult ;
    }
    public double newtonsMethod(double[] coefficients,boolean problemType,double[] intervals, double uncertainty,double[] range){
        double prev=(intervals[1]+intervals[0])/2 ;
        double cur=prev-(calculateDerivative(prev,coefficients)/calculateSecondDerivative(prev,coefficients));
        while(Math.abs(cur-prev)>uncertainty && cur<=intervals[1] && cur>=intervals[0]){
            prev=cur;
            cur=prev-(calculateDerivative(prev,coefficients)/calculateSecondDerivative(prev,coefficients));
        }
        return analyzeOutput(problemType,coefficients,range,cur);
    }
    public double secant(double[] coefficients,boolean problemType,double[] intervals, double uncertainty,double[] range){
        double prev1=intervals[0];
        double prev2=intervals[1];
        double cur=prev2-(calculateDerivative(prev2,coefficients)*(prev2-prev1))
                /(calculateDerivative(prev2,coefficients)-calculateDerivative(prev1,coefficients));
        while(Math.abs(cur-prev2)>uncertainty){
            prev1=prev2;
            prev2=cur;
            cur=prev2-(calculateDerivative(prev2,coefficients)*(prev2-prev1))
                    /(calculateDerivative(prev2,coefficients)-calculateDerivative(prev1,coefficients));
        }

        return analyzeOutput(problemType,coefficients,range,cur);
    }

    //Calculate the result of the function for the given coefficients
    public static double calculateFx(double x,double[] coefficients){
        int length = coefficients.length;
        double result = 0;
        for (int i = 0; i < length ; i++) {
            result += Math.pow(x,length-1-i) * coefficients[i];
        }
        return result ;
    }

    public double calculateDerivative(double x, double[] coefficients){
        double result = 0.0 ;
        result += 3 * coefficients[0] * Math.pow(x,2);
        result += 2 * coefficients[1] * x;
        result +=  coefficients[2];
        return result ;
    }
    public double calculateSecondDerivative(double x, double[] coefficients){
        double result = 0.0 ;
        result += 6 * coefficients[0] * x;
        result += 2 * coefficients[1];
        return result ;
    }
    public double analyzeOutput(boolean problemType,double[] coefficients,double[] range,double cur){
        // if the value we find is in the expected range
        if ( range[0] <= cur && cur <= range[1]){// The value is in range but is it the wanted value ?
            double confirmtype = calculateSecondDerivative(cur,coefficients);
            if(confirmtype < 0 && problemType) { // The value is local max and the wanted type is max
                Main.setStatusCode(1);
            }else if (confirmtype > 0 && (!problemType)){//the value is local min and the wanted type is min
                Main.setStatusCode(1);
            }else {
                Main.setStatusCode(6);
            }
        }else {// The value is not in the range, give boundaries, printout status code
            Main.setStatusCode(5);
        }
        return cur ;
    }
}

