/**
 * Created by yigitozgumus on 12/16/15.
 */

import java.util.*;
import java.io.*;

public class Main {

    // Data structures and Variable declarations
    static double[] coefficients;
    static double uncertainty;
    // if it is true -> max else min
    static boolean problemType ;
    static String type ;
    //Lower and upper bound values
    static double[] intervals ;
    static double[] range ;
    static int method;

    public static int getStatusCode() {
        return statusCode;
    }

    public static void setStatusCode(int statusCode) {
        Main.statusCode = statusCode;
    }

    static int statusCode = 0;
    // 0 => Bisection or Golden Section method, print the result.
    // 1 => The method found the wanted values
    // 5 => The value the method find is not in the given limits, so give the appropriate
    // 6 => The value the method find is in the range, but opposite of the problem type(problem asked the min but it is max etc. print the appropriate boundary)

    public static void main(String[] args) throws FileNotFoundException,UnsupportedEncodingException{
        //Start of the Program. write some pretty stuff
        getInput();
        Methods methods = new Methods();
        double localOptimum = 0.0 ;
        switch(method){
            case(1):localOptimum= methods.bisection(coefficients,problemType,intervals,uncertainty); break ;
            case(2):localOptimum= methods.goldenSection(coefficients,problemType,intervals,uncertainty); break ;
            case(3):localOptimum= methods.newtonsMethod(coefficients,problemType,intervals,uncertainty,range); break ;
            case(4):localOptimum= methods.secant(coefficients,problemType,intervals,uncertainty,range); break ;
        }
        writeResult(localOptimum,type,statusCode);
    }

    public static void getInput() throws FileNotFoundException,UnsupportedEncodingException{

        //Create the scanner
        Scanner in=new Scanner(new File("input.txt"));
        //Start hardcoded parsing. Yeaaah
        in.nextLine();
        type=in.next(); in.nextLine(); in.nextLine(); // Don't touch magic
        problemType = type.equalsIgnoreCase("max");

        in.nextLine();
        //Coefficients ========================================================
        coefficients = new double [4];
        //get the String split it and add it to the array
        String test = in.nextLine();
        String[] coefficientSplit = test.split(", ");
        for (int index = 0; index < coefficients.length ; index++) {
            coefficients[index] = Double.parseDouble(coefficientSplit[index]);
        }
        //Intervals ===========================================================
        in.nextLine(); in.nextLine();
        intervals = new double[2];
        range = new double[2];
        String luExtract = in.nextLine();
        String[] intervalSplit = luExtract.split(", ");
        for (int index = 0; index < intervals.length ; index++) {
            intervals[index] = Double.parseDouble(intervalSplit[index]);
            range[index] = Double.parseDouble(intervalSplit[index]);
        }
        //Uncertainty ========================================================
        in.nextLine();in.nextLine();
        uncertainty = in.nextDouble();
        //Method =============================================================
        in.nextLine();in.nextLine();in.nextLine();in.nextLine();
        method = in.nextInt();
        System.out.println("All inputs are parsed.");
    }
    public static void writeResult(double result, String type,int statusCode) throws FileNotFoundException, UnsupportedEncodingException {
        //calculate the values of the boundaries for other cases
        double lowerBound = Methods.calculateFx(range[0],coefficients);
        double upperBound = Methods.calculateFx(range[1],coefficients);
        double alternateResult ;
        PrintWriter writer = new PrintWriter("output.txt", "UTF-8");
        writer.println("================================ Output =====================================");
        writer.println("General Information==========================================================\n");
        writer.println("The range is from " + range[0] + " to " + range[1]);
        writer.println("The problem type is finding a local " + type +"ima");
        switch(method){
            case 1: writer.println("Bisection method is used\n"); break;
            case 2: writer.println("Golden Section method is used\n"); break;
            case 3: writer.println("Newton's method is used\n"); break;
            case 4: writer.println("Secant method is used\n"); break;
        }
        writer.println("Result=======================================================================");
        switch(statusCode){
            case 0 : // Bisection or Golden Section is used, printout the result
            case 1 :
                writer.println("The Local " + type +"ima of the function is :" + result );
                break ;
            case 5 : // not in the range
                writer.println("The value that the method find is :" + result + ".\nBut it is not in the range of the problem");
                if (problemType){ //local maximum
                    alternateResult = (lowerBound>upperBound?range[0]:range[1]);
                }else{ // local minimum
                    alternateResult = (lowerBound>upperBound?range[1]:range[0]);
                }
                writer.println("Therefore the output will be an appropriate boundary. \n" +
                      "The " + (alternateResult==range[0]?"Lower":"Upper") + " boundary point "  + alternateResult + " is the local " + type +"ima");
                break ;
            case 6 :
                writer.println("The result is in the range but it is a local " + (problemType ?"minima":"maxima") + " instead of " + type + "ima" );
                if (problemType){ //local maximum
                    alternateResult = (lowerBound>upperBound?range[0]:range[1]);
                }else{ // local minimum
                    alternateResult = (lowerBound>upperBound?range[1]:range[0]);
                }
                writer.println("Therefore the output will be an appropriate boundary. \n" +
                        "The " + (alternateResult==range[0]?"Lower":"Upper") + " boundary point "  + alternateResult + " is the local " + type +"ima");
                writer.println("The value that the method find as local " + (problemType ?"minima":"maxima") + " is :" + result );
                break ;
        }
        writer.close();

    }
}
